import requests
import json
import sys
import datetime

url = 'http://127.0.0.1:8000/graphql'
date_format="%Y%m%d_%H%M%S"

def make_a_query(query, variables):
    r = requests.post(url, json={'query': query , 'variables' : variables})
    print(r.status_code)
    print(r.text)
    json_data = json.loads(r.text)


###### List all instruments ##########
def query_all_intruments():
    query = """query {
      allInstruments {
        symbol
        name
      }
    }"""; 
    make_a_query(query, {})




###### Add Portfolio ##########
def add_portfolio(name):
    query = """
    mutation ($name: String!, $description: String!) {
        addPortfolio(portfolioName: $name, portfolioDescription: $description) {
            portfolio {
                name
            }
        }
    }"""

    timestamp=datetime.datetime.now().strftime(date_format)
    description =  name + 'created on ' + timestamp 
    data = {'name' : name, 'description' : description}
    make_a_query(query, json.dumps(data))


###### Add Trade ##########
def addTrade(portfolio_name, instrument_name):
    query = """
    mutation ($name: String!, $tradeBuyValue: Float!, $tradeSellValue: Float!, $volume: Int!, $instrument_name: String! $portfolio_name: String!) {
        addTrade(tradeName: $name, tradeBuyValue: $tradeBuyValue, tradeSellValue: $tradeSellValue, tradeVolume: $volume, parentInstrument: $instrument_name, parentPortfolio: $portfolio_name) {
            trade {
                name
            }
        }
    }"""

    timestamp=datetime.datetime.now().strftime(date_format)
    name = 'Trade_' + timestamp
    #portfolio_name = 'Portfolio_'+'Martha'
    #portfolio_name = 'Portfolio_'+'Roberta'
    #portfolio_name = 'Portfolio_'+'Johan'
    description = 'Trade for ' + name 
    data = {'name' : name, 
            'tradeBuyValue' : 1.2, 
            'tradeSellValue' : 100.5, 
            'volume' : 5, 
            'instrument_name' : instrument_name, 
            'portfolio_name' : portfolio_name}
    make_a_query(query, json.dumps(data))

#query_all_intruments()
portfolio_name='Portfolio_Richard'
add_portfolio(portfolio_name)
instrument_name='ABM'
addTrade(portfolio_name, instrument_name)
sys.exit()
###### Update Trade for sell ##########
print('UpdateTrade for sell')
query = """
mutation ($name: String!, $tradeSellValue: Float!) {
    updateTradeForSell(tradeName: $name, tradeSellValue: $tradeSellValue) {
        trade {
            name
        }
    }
}"""

timestamp='2020Jan06_134031'
name = 'Trade_' + timestamp
data = {'name' : name, 'tradeSellValue' : 3.5 }
make_a_query(query, json.dumps(data))
sys.exit()



###### Update Trade ##########
print('UpdateTrade')
query = """
mutation ($name: String!, $tradeSellValue: Float!, $tradeBuyValue: Float!, $volume: Int!, $instrument_name: String! $portfolio_name: String!) {
    updateTrade(tradeName: $name,  tradeBuyValue: $tradeBuyValue, tradeSellValue: $tradeSellValue, tradeVolume: $volume, parentInstrument: $instrument_name, parentPortfolio: $portfolio_name) {
        trade {
            name
        }
    }
}"""

timestamp=datetime.datetime.now().strftime(date_format)
timestamp='20200106_113614'
name = 'Trade_' + timestamp
portfolio_name = 'Portfolio_'+'Martha'
portfolio_name = 'Portfolio_'+'Roberta'
portfolio_name = 'Portfolio_'+'Johan'
description = 'Trade for ' + name 
data = {'name' : name, 
        'tradeBuyValue' : 1.5, 
        'tradeSellValue' : 0, 
        'volume' : 5, 
        'instrument_name' : 'ABM', 
        'portfolio_name' : portfolio_name}
make_a_query(query, json.dumps(data))


sys.exit(0)
###### Update Portfolio ##########

query = """
mutation ($name: String!, $description: String!) {
    updatePortfolio(portfolioName: $name, portfolioDescription: $description) {
        portfolio {
            name
        }
    }
}"""

name = 'Portofolio_'+'Johan'
name = 'Portfolio_'+'Richard'
description = 'Portfolio for ' + name 
data = {}
data = {'name' : name, 'description' : description}
variables = json.dumps(data)
make_a_query(query, variables)
