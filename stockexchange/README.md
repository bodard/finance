# Set-up of the environnement

With virtual env

```
virtualenv -p /usr/bin/python3 isoEnv
source isoEnv/bin/activate

python -V

# Install Django and Graphene with Django support
pip install django
pip install graphene_django
pip install django-extensions    # Used for loading the JSON data for instruments

```


# To create a new django project
Doc followed : https://docs.graphene-python.org/projects/django/en/latest/tutorial-relay/
```
django-admin.py startproject stockexchange .  
cd cookbook
django-admin.py startapp broker

cd ..
python ./manage.py migrate

```

# Preparing the DB
```
python manage.py migrate
python manage.py makemigrations
python manage.py migrate
#python manage.py loaddata ingredients_finance.json
python manage.py runscript load_instruments_data.py
```


# For already existing django project
```
python ./manage.py  runserver
```
In  127.0.0.1:8000/graphql
```
query {
  allIngredients {
    id
    name
    symbol
  }
}
```


# Running with docker-compose
* Must be a filled db.sqlite3 (step 'Preparing the DB')
```
docker-compose down
docker-compose build
docker-compose up
```
