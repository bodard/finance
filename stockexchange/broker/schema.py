import graphene
from graphene_django.types import DjangoObjectType
from stockexchange.broker.models import Portfolio, Trade, Instrument
from django.db.models import Avg, Sum, F, FloatField, ExpressionWrapper
import stockexchange.schema
import collections

def execute_mutation_updatePortfolio(portfolio_name):
    # exec mutation on portfolio so that portfolio_value can be computed
    query = """
    mutation ($name: String!, $description: String!) {
        updatePortfolio(portfolioName: $name, portfolioDescription: $description) {
            portfolio {
                name
                holdingValue
            }
        }
    }"""
    #name = parentPortfolio
    name = portfolio_name
    description = 'Portfolio for ' + name 
    data = {'name' : name, 'description' : description}

    schema = graphene.Schema(mutation=stockexchange.schema.Mutation)
    result = schema.execute(query, variable_values=data).data


class PortfolioType(DjangoObjectType):
    class Meta:
        model = Portfolio

class TradeType(DjangoObjectType):
    class Meta:
        model = Trade

class InstrumentType(DjangoObjectType):
    class Meta:
        model = Instrument

class Query(object):
    portfolio = graphene.Field(PortfolioType,
                                id=graphene.Int(),
                                name=graphene.String())
    all_portfolios = graphene.List(PortfolioType)
    
    all_trades = graphene.List(TradeType)
    
    all_instruments = graphene.List(InstrumentType)
    instrument = graphene.Field(InstrumentType,
                                iexId=graphene.Int(),
                                name=graphene.String(),
                                symbol=graphene.String())

    def resolve_portfolio(self, info, **kwargs):
        id = kwargs.get('id')
        name = kwargs.get('name')

        if id is not None:
            return Portfolio.objects.get(pk=id)

        if name is not None:
            return Portfolio.objects.get(name=name)

        return None
    
    def resolve_instrument(self, info, **kwargs):
        iexId = kwargs.get('iexId')
        symbol = kwargs.get('symbol')

        if symbol is not None:
            return Instrument.objects.get(symbol=symbol)
        
        if iexId is not None:
            return Instrument.objects.get(pk=iexId)

        return None
    
    def resolve_all_portfolios(self, info, **kwargs):
        return Portfolio.objects.all()

    def resolve_all_trades(self, info, **kwargs):
        return Trade.objects.all()
        #return Trade.objects.select_related('portfolio').all()
    
    def resolve_all_instruments(self, info, **kwargs):
        return Instrument.objects.all()


class AddPortfolio(graphene.Mutation):
    class Arguments:
        portfolioName = graphene.String(required=True)
        portfolioDescription = graphene.String(required=True)

    portfolio = graphene.Field(PortfolioType)

    def mutate(self, info, portfolioName, portfolioDescription):
        if Portfolio.objects.filter(name=portfolioName).exists():
            raise Exception('This portfolio name already exists.')
        else:
            pass
        _portfolio = Portfolio.objects.create(name=portfolioName,
                                            description=portfolioDescription,
                                            total_profit_loss=0,
                                            holding_value=0)

        return AddPortfolio(portfolio=_portfolio)

class UpdatePortfolio(graphene.Mutation):
    class Arguments:
        portfolioName = graphene.String(required=True)
        portfolioDescription = graphene.String(required=True)

    portfolio = graphene.Field(PortfolioType)

    def mutate(self, info, portfolioName, portfolioDescription):
        if Portfolio.objects.filter(name=portfolioName).exists():
            _portfolio=Portfolio.objects.get(name=portfolioName)
        else:
            raise Exception('This portfolio does not exist.')
        
        _portfolio.description = portfolioDescription
        _portfolio.name = portfolioName
        # Update the profit_loss by summing the profit_loss of all the trade of the portfolio
        _portfolio.total_profit_loss=Trade.objects.filter(portfolio=_portfolio.id).aggregate(Sum('profit_loss'))['profit_loss__sum']
        # Update the holding value with the value of the instrument * volume for the trade that have buy only (sell_value is Null)
        _portfolio.holding_value = Trade.objects.filter(portfolio=_portfolio.id).filter(sell_value__isnull=True).annotate(total=ExpressionWrapper(F('buy_value')*F('volume'), output_field = FloatField() )).values('total').aggregate(Sum('total'))['total__sum']
        _portfolio.save()
        return UpdatePortfolio(portfolio=_portfolio)

class AddTrade(graphene.Mutation):
    class Arguments:
        tradeName = graphene.String(required=True)
        tradeVolume = graphene.Int(required=True)
        tradeSellValue = graphene.Float(required=False)
        tradeBuyValue = graphene.Float(required=True)
        parentPortfolio = graphene.String(required=True) 
        parentInstrument = graphene.String(required=True)  

    trade = graphene.Field(TradeType)

    def mutate(self, info, tradeName, tradeVolume, tradeSellValue, tradeBuyValue, parentPortfolio, parentInstrument):
        if Trade.objects.filter(name=tradeName).exists():
            raise Exception('This tradename is already used.')
        else:
            pass
        if Portfolio.objects.filter(name=parentPortfolio).exists():
            pass
        else:
            raise Exception('This portfolio does not exist.')
        if Instrument.objects.filter(symbol=parentInstrument).exists():
            pass
        else:
            raise Exception('This instrument does not exist.')
            
        _trade = Trade.objects.create(name=tradeName,
                        portfolio=Portfolio.objects.get(name=parentPortfolio),
                        instrument=Instrument.objects.get(symbol=parentInstrument),
                        volume=tradeVolume,
                        #sell_value=tradeSellValue,
                        sell_value=None,
                        buy_value=tradeBuyValue,
                        profit_loss=None
                        )
        
        execute_mutation_updatePortfolio(parentPortfolio)
        return AddTrade(trade=_trade)

class UpdateTrade(graphene.Mutation):
    class Arguments:
        tradeName = graphene.String(required=True)
        tradeVolume = graphene.Int(required=True)
        tradeSellValue = graphene.Float(required=True)
        tradeBuyValue = graphene.Float(required=True)
        parentPortfolio = graphene.String(required=True) 
        parentInstrument = graphene.String(required=True)  
    trade = graphene.Field(TradeType)

    def mutate(self, info, tradeName, tradeVolume, tradeSellValue, tradeBuyValue, parentPortfolio, parentInstrument):
        if Trade.objects.filter(name=tradeName).exists():
            pass
        else:
            raise Exception('This tradename does not exist.')
        if Portfolio.objects.filter(name=parentPortfolio).exists():
            pass
        else:
            raise Exception('This portfolio does not exist.')
        if Instrument.objects.filter(symbol=parentInstrument).exists():
            pass
        else:
            raise Exception('This instrument does not exist.')
        
        
        _trade=Trade.objects.get(name=tradeName)
        _trade.name = tradeName
        _trade.volume = tradeVolume
        _trade.sell_value = tradeSellValue
        _trade.buy_value = tradeBuyValue
        # Compute profit/loss of this trade if relevant
        if tradeSellValue :
            _trade.profit_loss = (tradeSellValue - tradeBuyValue)*tradeVolume
        _trade.portfolio = Portfolio.objects.get(name=parentPortfolio)
        _trade.instrument = Instrument.objects.get(symbol=parentInstrument)
        _trade.save()
        
        execute_mutation_updatePortfolio(parentPortfolio)

        return UpdateTrade(trade=_trade)

class UpdateTradeForSell(graphene.Mutation):
    class Arguments:
        tradeName = graphene.String(required=True)
        tradeSellValue = graphene.Float(required=True)
    trade = graphene.Field(TradeType)

    def mutate(self, info, tradeName, tradeSellValue):
        if Trade.objects.filter(name=tradeName).exists():
            pass
        else:
            raise Exception('This tradename does not exist.')
        _trade=Trade.objects.get(name=tradeName)
        # exec mutation on update trade to use the same class
        query = """
        mutation ($name: String!, $tradeBuyValue: Float!, $tradeSellValue: Float!, $volume: Int!, $instrument_name: String! $portfolio_name: String!) {
            updateTrade(tradeName: $name, tradeBuyValue: $tradeBuyValue, tradeSellValue: $tradeSellValue, tradeVolume: $volume, parentInstrument: $instrument_name, parentPortfolio: $portfolio_name) {
                trade {
                    name
                }
            }
        }"""
        data = {'name' : _trade.name, 
                'tradeBuyValue' : _trade.buy_value, 
                'tradeSellValue' : tradeSellValue, 
                'volume' : _trade.volume, 
                'instrument_name' : _trade.instrument, 
                'portfolio_name' : _trade.portfolio}

        schema = graphene.Schema(mutation=stockexchange.schema.Mutation)
        result = schema.execute(query, variable_values=data).data
        # Get portfolio name or holding_value
        print(result)
        return UpdateTradeForSell(trade=_trade)


class Mutation(object):
    add_portfolio = AddPortfolio.Field()
    update_portfolio = UpdatePortfolio.Field()
    add_trade = AddTrade.Field()
    update_trade = UpdateTrade.Field()
    update_trade_for_sell = UpdateTradeForSell.Field()

