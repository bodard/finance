from django.contrib import admin
from stockexchange.broker.models import Portfolio, Trade, Instrument

admin.site.register(Portfolio)
admin.site.register(Trade)
admin.site.register(Instrument)
