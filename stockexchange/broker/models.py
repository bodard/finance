from django.db import models

class Portfolio(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)
    holding_value = models.FloatField(null=True)
    total_profit_loss = models.FloatField(null=True)

    def __str__(self):
        return self.name

class Instrument(models.Model):
    name = models.CharField(max_length=100)
    symbol = models.CharField(max_length=100)
    date = models.DateField()
    isEnabled = models.BooleanField()
    type = models.CharField(max_length=100)
    iexId = models.CharField(max_length=100, primary_key=True)

    def __str__(self):
        return self.symbol

class Trade(models.Model):
    name = models.CharField(max_length=100)
    volume = models.PositiveIntegerField()
    sell_value = models.FloatField(null=True)
    buy_value = models.FloatField()
    profit_loss = models.FloatField(null=True)
    portfolio = models.ForeignKey(
        Portfolio, related_name='trades', on_delete=models.CASCADE)
    instrument = models.ForeignKey(
        Instrument, related_name='trades', on_delete=models.CASCADE)

    def __str__(self):
        return self.name

