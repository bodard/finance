## RUN phase
# Preparation : download of the sqlite file
There must be a filled sqlite at the root of the repo ./db.sqlite3 (step 'Preparing the DB').
A database is filled with the instruments table and empty tables for trade and portfolio ; it can be download from a browser with the following address : https://drive.google.com/file/d/1HjHZpcNmDUKIFA1oMZQw2sOJ5hC1G_0r/view?usp=sharing
Then, copy it at the root of the repo and rename it db.sqlite3

# Running with docker-compose
```
docker-compose down
docker-compose build
docker-compose up
```

## USAGE
The endpoint of the API is 127.0.0.1:8000/graphql

# With CURL
Syntax :
```
ENDPOINT='http://127.0.0.1:8000/graphql'
curl \
  -X GET \
  -H "Content-Type: application/json" \
  --data '{ "query": "'"${PAYLOAD}"'" }' \
  ${ENDPOINT} | jq
```

```
# Query all instruments
PAYLOAD="{ allInstruments { name, symbol, type,  date, iexId,  isEnabled }}"

# Query all instruments, with all the trades they are link to
PAYLOAD="{ allInstruments { symbol , trades { id , name } } }"

# Query only one instrument :
PAYLOAD="{ instrument(iexId: 272) { name, symbol, isEnabled } }"

# all portfolios 
PAYLOAD="{ allPortfolios { id, name, description } }"

# select one portfolio with the id
PAYLOAD="{ portfolio(id: 1) { name , trades {id,name}}}"
```


# From the browser 
Query for fetching all instruments

```
query {
  allInstruments {
    symbol
    name
  }
}
```


# Query only one instrument :
Query for fetching a single instrument, selectable via id or symbol
```
query {
  instrument(symbol: "ABM") {
    name
    symbol
    isEnabled
  }
}

query {
  instrument(iexId: 20) {
    isEnabled
    name
    symbol
  }
}
```

Query for fetching all portfolios
# Query all portfolios
```
query {
  allPortfolios {
    id
    name
    description
    holdingValue 
  }
}

```

# Query for fetching a single portfolio, selectable via id or name
```
query {
  portfolio(name: "Portfolio_Richard") {
    id
    name
    description
    holdingValue
    totalProfitLoss
    trades{name, buyValue, sellValue, profitLoss}
  }
}
```

# Query for fetching all the trades, with the instrument and portfolios they are linked to
```
query {
  allTrades {
    id
    name
    volume
    instrument {
      iexId
      symbol
    }
    portfolio {
      id
      name
    }
    sellValue
    buyValue
  }
}
```



# Mutation for creating a portfolio with fields name, description
```
mutation  {
    addPortfolio(portfolioName:"Portfolio_Richard", portfolioDescription:"The portfolio for Richard") {
        portfolio {
            name
        }
    }
}
```

# Mutation for editing a portfolio with fields name, description
```
mutation  {
    updatePortfolio(portfolioName:"Portfolio_Richard", portfolioDescription:"The portfolio for Richard, for his trading hobby") {
        portfolio {
            name
        }
    }
}
```

# Mutation for creating a trade, with fields for portfolio, instrument, volume and value
```
mutation  {
    addTrade(tradeName:"trade_caoutchouc", tradeSellValue:0.00, tradeBuyValue:105.50, tradeVolume:5,parentInstrument:"ABM", parentPortfolio:"Portfolio_Richard") {
        trade {
            name
        }
    }
}
```

# Mutation for editing a trade, with same fields as create.
```
mutation  {
    updateTrade(tradeName:"trade_caoutchouc", tradeSellValue:100.00, tradeBuyValue:105.50, tradeVolume:5,parentInstrument:"AAON", parentPortfolio:"Portfolio_Richard") {
        trade {
            name
        }
    }
}
```

# Mutation for editing a trade : sell the instrument
```
mutation  {
    updateTradeForSell(tradeName:"trade_caoutchouc", tradeSellValue:100.00) {
        trade {
            name
        }
    }
}
```


## FOR INFORMATION  : BUILD phase 
# Set-up of the environnement

With virtual env

```
virtualenv -p /usr/bin/python3 isoEnv
source isoEnv/bin/activate

python -V

# Install Django and Graphene with Django support
#pip install django
#pip install graphene_django
#pip install django-extensions    # Used for loading the JSON data for instruments

pip install -r requirements.txt

```


# Creation of a new django project
Follow this https://docs.graphene-python.org/projects/django/en/latest/tutorial-relay/
```
django-admin.py startproject stockexchange .  
cd stockexchange
django-admin.py startapp broker

cd ..
python ./manage.py migrate

```

# Preparation of the database
```
#   rm sql
#rm db.sqlite3
#rm stockexchange/broker/migrations/00*
#rm stockexchange/broker/migrations/__pycache__/*

python manage.py migrate
python manage.py makemigrations
python manage.py migrate
python manage.py runscript load_instruments_data.py
```


# Running locally with virtualenv
```
python ./manage.py  runserver
```

