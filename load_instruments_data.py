import json, sys
from stockexchange.broker.models import Instrument

filename="instruments_data.json"
with open(filename, "r") as read_file:
    list_data = json.load(read_file)
    
for data in list_data:
    #Instrument.objects.create(**data)
    obj, created = Instrument.objects.get_or_create(**data)
    if created :
        print("Object created for : " + str(data))
    else:
        print("An object exists for : " + str(data))

sys.exit(0)
